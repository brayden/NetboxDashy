﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using NetboxDashyCli.Models;
using NetboxDashyCli.Models.Netbox;
using Newtonsoft.Json;
using NLog;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace NetboxDashyCli
{
    public class DashyIntegration
    {
        public ConfigModel Config { get; set; }
        public DashyModel DashyConfig { get; set; }
        public Logger Logger = LogManager.GetCurrentClassLogger();

        public DashyIntegration(string configPath)
        {
            if (!File.Exists(configPath))
            {
                Logger.Error($"Config doesn't exist at {configPath}");
                Environment.Exit(1);
            }
            Config = JsonConvert.DeserializeObject<ConfigModel>(File.ReadAllText(configPath));
            Healthchecks.Start(Config.Healthchecks).Wait();
            Logger.Info($"Config loaded! Netbox URL is: {Config.NetboxUrl}");

            if (!File.Exists(Config.DashyConfigPath))
            {
                Logger.Error($"Dashy config doesn't exist at {Config.DashyConfigPath}");
                Healthchecks.Fail(Config.Healthchecks, $"Dashy config doesn't exist at {Config.DashyConfigPath}")
                    .Wait();
                Environment.Exit(1);
            }

            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .Build();
            DashyConfig = deserializer.Deserialize<DashyModel>(File.ReadAllText(Config.DashyConfigPath));
            Logger.Info("Syncing Dashy sections with Netbox");
            try
            {
                SyncDashySections().Wait();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                Healthchecks.Fail(Config.Healthchecks, e.StackTrace).Wait();
                Environment.Exit(1);
            }

            Logger.Info("Syncing Netbox data with Dashy");
            try
            {
                SyncNetboxItemsToDashy().Wait();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                Healthchecks.Fail(Config.Healthchecks, e.StackTrace).Wait();
                Environment.Exit(1);
            }

            Healthchecks.Finish(Config.Healthchecks).Wait();
        }

        public async Task SyncDashySections()
        {
            Logger.Info("Getting Netbox section selections");
            // Authorization headers are not preserved on redirects, so don't follow them in this case
            var client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            client.BaseAddress = Config.NetboxUrl;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", Config.ApiKey);
            var response = await client.GetAsync("/api/extras/custom-fields/");
            response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            var customFields = JsonConvert.DeserializeObject<CustomFieldsModel>(json);
            var field = customFields.Results.FirstOrDefault(cf => cf.Name == "dashy_section");
            if (field == null)
            {
                Logger.Error("Failed to find Netbox custom field called 'dashy_section'");
                Environment.Exit(1);
            }
            Logger.Info("Comparing Netbox section selections with Dashy config");
            List<string> dashySections = DashyConfig.Sections
                .Where(section => !Config.IgnoredSections.Contains(section.Name)).Select(section => section.Name)
                .ToList();
            if(field.Choices.SequenceEqual(dashySections))
            {
                Logger.Info("Netbox is up to date already.");
                return;
            }
            Logger.Info($"Updating Netbox with: {string.Join(",", dashySections)}");
            dynamic payload = new ExpandoObject();
            payload.choices = dashySections;
            string jsonPayload = JsonConvert.SerializeObject(payload);
            var patchResponse = await client.PatchAsync($"/api/extras/custom-fields/{field.Id}/", new StringContent(jsonPayload, Encoding.UTF8, "application/json"));
            patchResponse.EnsureSuccessStatusCode();
        }
        
        public async Task SyncNetboxItemsToDashy()
        {
            Dictionary<string, List<Dictionary<string, object>>> dashyItems =
                new Dictionary<string, List<Dictionary<string, object>>>();
            Logger.Info("Getting Netbox VMs");
            // Authorization headers are not preserved on redirects, so don't follow them in this case
            var client = new HttpClient(new HttpClientHandler { AllowAutoRedirect = false });
            client.BaseAddress = Config.NetboxUrl;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", Config.ApiKey);
            string url = "/api/virtualization/virtual-machines/?cf_in_dashy=true";
            HttpResponseMessage response;
            while (true)
            {
                response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                var virtualMachines = JsonConvert.DeserializeObject<VirtualMachinesModel>(json);
                foreach (var vm in virtualMachines.Results)
                {
                    Logger.Info($"Looking at {vm.Name}");
                    if (vm.CustomFields.DashySection == null)
                    {
                        Logger.Error($"{vm.Name} Dashy section is null, skipping");
                        continue;
                    }

                    if (Config.IgnoredSections.Contains(vm.CustomFields.DashySection))
                    {
                        Logger.Error($"{vm.Name}'s section is ignored in the config");
                        continue;
                    }
    
                    if (!dashyItems.ContainsKey(vm.CustomFields.DashySection))
                    {
                        if (DashyConfig.Sections.All(s => s.Name != vm.CustomFields.DashySection))
                        {
                            Logger.Error($"{vm.Name}'s section '{vm.CustomFields.DashySection}' does not exist in the Dashy config, skipping");
                            continue;
                        }
                        
                        dashyItems.Add(vm.CustomFields.DashySection, new List<Dictionary<string, object>>());
                    }
                    
                    if (vm.CustomFields.DashyData == null)
                    {
                        Logger.Error($"{vm.Name}'s Dashy data is null, skipping");
                        continue;
                    }
                    
                    dashyItems[vm.CustomFields.DashySection].Add(ParseDashyData(vm.CustomFields.DashyData));
                }

                if (virtualMachines.Next == null)
                {
                    break;
                }

                url = virtualMachines.Next.PathAndQuery;
            }

            Logger.Info("Getting Netbox devices");
            url = "/api/dcim/devices/?cf_in_dashy=true";
            while (true)
            {
                response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                var devices = JsonConvert.DeserializeObject<DevicesModel>(json);
                foreach (var device in devices.Results)
                {
                    Logger.Info($"Looking at {device.Name}");
                    if (device.CustomFields.DashySection == null)
                    {
                        Logger.Error($"{device.Name} Dashy section is null, skipping");
                        continue;
                    }

                    if (!dashyItems.ContainsKey(device.CustomFields.DashySection))
                    {
                        if (DashyConfig.Sections.All(s => s.Name != device.CustomFields.DashySection))
                        {
                            Logger.Error($"{device.Name}'s section '{device.CustomFields.DashySection}' does not exist in the Dashy config, skipping");
                            continue;
                        }
                    
                        dashyItems.Add(device.CustomFields.DashySection, new List<Dictionary<string, object>>());
                    }

                    if (device.CustomFields.DashyData == null)
                    {
                        Logger.Error($"{device.Name}'s Dashy data is null, skipping");
                        continue;
                    }

                    dashyItems[device.CustomFields.DashySection].Add(ParseDashyData(device.CustomFields.DashyData));
                }

                if (devices.Next == null)
                {
                    break;
                }

                url = devices.Next.PathAndQuery;
            }
            
            Logger.Info("Getting Netbox services");
            url = "/api/ipam/services/?cf_in_dashy=true";
            while (true)
            {
                response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                var services = JsonConvert.DeserializeObject<ServicesModel>(json);
                foreach (var service in services.Results)
                {
                    if (service.Device != null)
                    {
                        Logger.Info($"Looking at {service.Name} on {service.Device.name}");
                    }
                    else if(service.VirtualMachine != null)
                    {
                        Logger.Info($"Looking at {service.Name} on {service.VirtualMachine.Name}");
                    }
                    else
                    {
                        Logger.Info($"Looking at {service.Name}");
                    }
                    if (service.CustomFields.DashySection == null)
                    {
                        Logger.Error($"{service.Name} Dashy section is null, skipping");
                        continue;
                    }

                    if (!dashyItems.ContainsKey(service.CustomFields.DashySection))
                    {
                        if (DashyConfig.Sections.All(s => s.Name != service.CustomFields.DashySection))
                        {
                            Logger.Error($"{service.Name}'s section '{service.CustomFields.DashySection}' does not exist in the Dashy config, skipping");
                            continue;
                        }
                    
                        dashyItems.Add(service.CustomFields.DashySection, new List<Dictionary<string, object>>());
                    }

                    if (service.CustomFields.DashyData == null)
                    {
                        Logger.Error($"{service.Name}'s Dashy data is null, skipping");
                        continue;
                    }

                    dashyItems[service.CustomFields.DashySection].Add(ParseDashyData(service.CustomFields.DashyData));
                }

                if (services.Next == null)
                {
                    break;
                }

                url = services.Next.PathAndQuery;
            }
            foreach (var section in dashyItems.Keys)
            {
                DashyConfig.Sections.Find(s => s.Name == section).Items = dashyItems[section];
            }

            var serializer = new SerializerBuilder()
                .WithNamingConvention(CamelCaseNamingConvention.Instance)
                .ConfigureDefaultValuesHandling(DefaultValuesHandling.OmitDefaults | DefaultValuesHandling.OmitNull)
                .Build();
            var yaml = serializer.Serialize(DashyConfig);
            await File.WriteAllTextAsync(Config.DashyConfigPath, yaml);
            Logger.Info($"Wrote the new Dashy config to {Config.DashyConfigPath}");
        }

        // Netbox is inconsistent with whether it chooses to serialize the data
        public static Dictionary<string, object> ParseDashyData(object dashyData)
        {
            return dashyData switch
            {
                string dashyJson => JsonConvert.DeserializeObject<Dictionary<string, object>>(dashyJson),
                Dictionary<string, object> dashyDict => dashyDict,
                _ => null
            };
        }
    }
}