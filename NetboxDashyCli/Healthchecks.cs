﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using NLog;

namespace NetboxDashyCli;

public static class Healthchecks
{
    public static async Task Start(string url)
    {
        var logger = LogManager.GetCurrentClassLogger();
        if (string.IsNullOrEmpty(url))
        {
            logger.Info("Healthchecks URL is null or empty, not going to do anything");
            return;
        }
        using var client = new HttpClient();
        
        HttpResponseMessage response;
        try
        {
            response = await client.GetAsync(url + "/start");
        }
        catch (Exception e)
        {
            logger.Error(e);
            return;
        }
            
        logger.Info($"Reported to healthchecks that we've started, got {response.StatusCode} in response");
    }
    
    public static async Task Finish(string url)
    {
        var logger = LogManager.GetCurrentClassLogger();
        if (string.IsNullOrEmpty(url))
        {
            logger.Info("Healthchecks URL is null or empty, not going to do anything");
            return;
        }
        using var client = new HttpClient();
        
        HttpResponseMessage response;
        try
        {
            response = await client.GetAsync(url);
        }
        catch (Exception e)
        {
            logger.Error(e);
            return;
        }
            
        logger.Info($"Reported to healthchecks that we've finished, got {response.StatusCode} in response");
    }
    
    public static async Task Fail(string url, string message)
    {
        var logger = LogManager.GetCurrentClassLogger();
        if (string.IsNullOrEmpty(url))
        {
            logger.Info("Healthchecks URL is null or empty, not going to do anything");
            return;
        }
        using var client = new HttpClient();
        
        HttpResponseMessage response;
        try
        {
            response = await client.PostAsync(url + "/fail", new StringContent(message));
        }
        catch (Exception e)
        {
            logger.Error(e);
            return;
        }
            
        logger.Info($"Reported to healthchecks that we've failed, got {response.StatusCode} in response");
    }
}