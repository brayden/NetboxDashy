﻿using System;
using System.Collections.Generic;

namespace NetboxDashyCli.Models
{
    public class ConfigModel
    {
        public string ApiKey { get; set; }
        public Uri NetboxUrl { get; set; }
        public string DashyConfigPath { get; set; }
        public List<string> IgnoredSections { get; set; } = new();
        public string Healthchecks { get; set; } = null;
    }
}