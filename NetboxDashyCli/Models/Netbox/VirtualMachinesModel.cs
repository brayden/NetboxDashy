﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NetboxDashyCli.Models.Netbox
{
    public class VirtualMachinesModel
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next")]
        public Uri Next { get; set; }

        [JsonProperty("previous")]
        public Uri Previous { get; set; }

        [JsonProperty("results")]
        public List<VirtualMachineResult> Results { get; set; }
    }

    public class VirtualMachineResult
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tenant")]
        public object Tenant { get; set; }

        [JsonProperty("vcpus")]
        public long VCPUs { get; set; }

        [JsonProperty("memory")]
        public long Memory { get; set; }

        [JsonProperty("disk")]
        public long Disk { get; set; }

        [JsonProperty("comments")]
        public string Comments { get; set; }

        [JsonProperty("custom_fields")]
        public VirtualMachineCustomFields CustomFields { get; set; }

        [JsonProperty("created")]
        public DateTimeOffset Created { get; set; }

        [JsonProperty("last_updated")]
        public DateTimeOffset LastUpdated { get; set; }
    }

    public class VirtualMachineCustomFields
    {
        [JsonProperty("zabbix_agent_deployed")]
        public bool? ZabbixAgentDeployed { get; set; }

        [JsonProperty("in_dashy")]
        public bool InDashy { get; set; }

        [JsonProperty("dashy_section")]
        public string DashySection { get; set; }

        [JsonProperty("dashy_data")]
        public object DashyData { get; set; }
    }
}
