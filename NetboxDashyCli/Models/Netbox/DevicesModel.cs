﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NetboxDashyCli.Models.Netbox
{
    public class DevicesModel
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next")]
        public Uri Next { get; set; }

        [JsonProperty("previous")]
        public Uri Previous { get; set; }

        [JsonProperty("results")]
        public List<DeviceResult> Results { get; set; }
    }

    public class DeviceResult
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("tenant")]
        public object Tenant { get; set; }

        [JsonProperty("serial")]
        public string Serial { get; set; }

        [JsonProperty("asset_tag")]
        public string AssetTag { get; set; }

        [JsonProperty("position")]
        public long? Position { get; set; }

        [JsonProperty("primary_ip6")]
        public object PrimaryIp6 { get; set; }

        [JsonProperty("virtual_chassis")]
        public object VirtualChassis { get; set; }

        [JsonProperty("vc_position")]
        public object VcPosition { get; set; }

        [JsonProperty("vc_priority")]
        public object VcPriority { get; set; }

        [JsonProperty("comments")]
        public string Comments { get; set; }

        [JsonProperty("custom_fields")]
        public CustomFields CustomFields { get; set; }

        [JsonProperty("created")]
        public DateTimeOffset Created { get; set; }

        [JsonProperty("last_updated")]
        public DateTimeOffset LastUpdated { get; set; }
    }

    public class CustomFields
    {
        [JsonProperty("in_dashy")]
        public bool InDashy { get; set; }

        [JsonProperty("dashy_section")]
        public string DashySection { get; set; }

        [JsonProperty("dashy_data")]
        public object DashyData { get; set; }
    }
}
