﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NetboxDashyCli.Models.Netbox
{
    public class ServicesModel
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next")]
        public Uri Next { get; set; }

        [JsonProperty("previous")]
        public Uri Previous { get; set; }

        [JsonProperty("results")]
        public List<ServicesResult> Results { get; set; }
    }

    public class ServicesResult
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("device")]
        public dynamic Device { get; set; }

        [JsonProperty("virtual_machine")]
        public VirtualMachine VirtualMachine { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ports")]
        public List<long> Ports { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("tags")]
        public List<object> Tags { get; set; }

        [JsonProperty("custom_fields")]
        public ServicesCustomFields CustomFields { get; set; }

        [JsonProperty("created")]
        public DateTimeOffset Created { get; set; }

        [JsonProperty("last_updated")]
        public DateTimeOffset LastUpdated { get; set; }
    }

    public class ServicesCustomFields
    {
        [JsonProperty("in_dashy")]
        public bool InDashy { get; set; }

        [JsonProperty("dashy_section")]
        public string DashySection { get; set; }

        [JsonProperty("dashy_data")]
        public object DashyData { get; set; }
    }

    public class VirtualMachine
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
