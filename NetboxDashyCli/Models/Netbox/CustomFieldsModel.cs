﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NetboxDashyCli.Models.Netbox
{
    public class CustomFieldsModel
    {
        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("next")]
        public Uri Next { get; set; }

        [JsonProperty("previous")]
        public Uri Previous { get; set; }

        [JsonProperty("results")]
        public List<CustomFieldResult> Results { get; set; }
    }

    public class CustomFieldResult
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("content_types")]
        public List<string> ContentTypes { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("required")]
        public bool ResultRequired { get; set; }

        [JsonProperty("weight")]
        public long Weight { get; set; }

        [JsonProperty("choices")]
        public List<string> Choices { get; set; }
    }
}
