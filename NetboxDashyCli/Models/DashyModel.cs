﻿using System.Collections.Generic;
using YamlDotNet.Serialization;

namespace NetboxDashyCli.Models
{
    public class DashyModel
    {
        public PageInfo PageInfo { get; set; }
        public AppConfig AppConfig { get; set; }
        public List<Section> Sections { get; set; }
    }

    public class PageInfo
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<NavLink> NavLinks { get; set; }
        public string FooterText { get; set; }
        public string Logo { get; set; }
    }

    public class NavLink
    {
        public string Title { get; set; }
        public string Path { get; set; }
    }

    public class AppConfig
    {
        public string Language { get; set; }
        public string StartingView { get; set; } 
        public bool StatusCheck { get; set; }
        public int StatusCheckInterval { get; set; }
        public string BackgroundImg { get; set; }
        public bool EnableFontAwesome { get; set; }
        public string FontAwesomeKey { get; set; }
        public string FaviconApi { get; set; }
        public object Auth { get; set; }
        public string Layout { get; set; }
        public string IconSize { get; set; }
        public string Theme { get; set; }
        public List<string> CssThemes { get; set; }
        public object CustomColors { get; set; }
        public object ExternalStyleSheet { get; set; }
        public string CustomCss { get; set; }
        public HideComponents HideComponents { get; set; }
        public bool EnableMultiTasking { get; set; }
        public bool AllowConfigEdit { get; set; }
        public bool EnableErrorReporting { get; set; }
        public string SentryDsn { get; set; }
        public bool DisableUpdateChecks { get; set; }
        public bool DisableServiceWorker { get; set; }
        public bool DisableContextMenu { get; set; }
    }

    public class HideComponents
    {
        public bool HideHeading { get; set; }
        public bool HideNav { get; set; }
        public bool HideSearch { get; set; }
        public bool HideSettings { get; set; }
        public bool HideFooter { get; set; }
        public bool HideSplashScreen { get; set; }
    }

    public class Section
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public DisplayData DisplayData { get; set; }
        public List<Dictionary<string, object>> Items { get; set; } = new();
        public dynamic Widgets { get; set; }
    }

    public class DisplayData
    {
        public bool Collapsed { get; set; }
        public string Color { get; set; }
        public string CustomStyles { get; set; }
        public string ItemSize { get; set; }
        public int Rows { get; set; }
        public int Cols { get; set; }
        public string SectionLayout { get; set; }
        public int ItemCountX { get; set; }
        public int ItemCountY { get; set; }
        public List<string> HideForUsers { get; set; }
        public List<string> ShowForUsers { get; set; }
        public bool HideForGuests { get; set; }
    }
}